comp=gcc -std=c99 -g

default : 4laba.o
	$(comp) -o exe 4laba.o

4laba.o : 4laba.c
	$(comp) -c 4laba.c

clean :
	rm *.o exe
