//Автор: Волков Евгений
// Лабораторная работа №4 часть 1
//
// Ввод в консоль  ./4laba_2 <имя файла>    // ./4laba_2  "input.txt"
// Считывает строки из входного файла и записывает в выходной файл строки, заканчивающиеся цифрами;

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char* argv[]) {
    FILE *fp, *fpw;
    int Counter = 0, temp = 0;
    int strCounter = 0;
    char c, *str;

    fpw = fopen("output.txt", "w+");

    if ((fp = fopen(argv[1], "r")) == NULL) {
        printf("Не удается открыть файл.\n");
        exit(1);
    }

    while (!feof(fp)) {
        c = (char)fgetc(fp); 
        Counter++;
        printf("%c", c);
    }

    str = malloc(sizeof(char) * Counter);
    rewind(fp);
    printf("----------------\n");

    while (fgets(str, Counter, fp) != NULL) {
        temp = (strlen(str) - 2);
        if (str[temp] >= '0' && str[temp] <= '9') {
            strCounter++;
            fprintf(fpw, "%s\n", str);
        }
    }

    if (strCounter != 0) {
        printf( "Выходной файл\n" );
        rewind(fpw);
        while (!feof(fpw)) {
            fgets(str, Counter, fpw);
            printf("%s", str);
        }
    } else {
        printf("----------Строк с заданным условием не найдено--------\n");
    }

    fclose(fpw);
    fclose(fp);
    free(str);
    return 0;
}
